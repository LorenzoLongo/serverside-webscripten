<?php

/* layout.twig */
class __TwigTemplate_89abaa549d9a9aa7579368186244f968f7c303574bb5d930f24631c1444a1e68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javaScript' => array($this, 'block_javaScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7 ]><html class=\"oldie ie6\" lang=\"en\"><![endif]-->
<!--[if IE 7 ]><html class=\"oldie ie7\" lang=\"en\"><![endif]-->
<!--[if IE 8 ]><html class=\"oldie ie8\" lang=\"en\"><![endif]-->
<!--[if IE 9 ]><html class=\"ie9\" lang=\"en\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang=\"en\"><!--<![endif]-->
<head>

\t<title>";
        // line 9
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "</title>

\t<meta charset=\"UTF-8\" />
\t<meta name=\"viewport\" content=\"width=520\" />
\t<meta http-equiv=\"cleartype\" content=\"on\" />
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />

\t<!--[if lt IE 9]><script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script><![endif]-->

\t<link rel=\"stylesheet\" media=\"screen\" href=\"css/reset.css\" />
\t<link rel=\"stylesheet\" media=\"screen\" href=\"css/screen.css\" />

\t";
        // line 21
        $this->displayBlock('javaScript', $context, $blocks);
        // line 24
        echo "
</head>
<body>

\t<div id=\"siteWrapper\">

\t\t<!-- header -->
\t\t<header>
\t\t\t<h1><a href=\"index.php\">Todolist</a></h1>
\t\t</header>

\t\t<!-- content -->
\t\t<section>


\t\t\t";
        // line 39
        $this->displayBlock('content', $context, $blocks);
        // line 41
        echo "
\t\t</section>

        \t\t<!-- footer -->
        \t\t<footer>
        \t\t\t<p>&copy; 2015, <a href=\"http://www.ikdoeict.be/\" title=\"IkDoeICT.be\">IkDoeICT.be</a></p>
        \t\t</footer>

        \t</div>

</body>
</html>";
    }

    // line 21
    public function block_javaScript($context, array $blocks = array())
    {
        // line 22
        echo "
\t";
    }

    // line 39
    public function block_content($context, array $blocks = array())
    {
        // line 40
        echo "\t\t\t";
    }

    public function getTemplateName()
    {
        return "layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 40,  90 => 39,  85 => 22,  82 => 21,  67 => 41,  65 => 39,  48 => 24,  46 => 21,  31 => 9,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "layout.twig", "D:\\wamp64\\www\\PHP\\2018-19\\labo5_LorenzoLongo\\labo5\\labo4_LorenzoLongo\\labo4\\basisbestanden\\www_\\templates\\layout.twig");
    }
}

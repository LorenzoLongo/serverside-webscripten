<?php

/* delete.twig */
class __TwigTemplate_154a1b3b74ca19765e57b2fdcca4d2aa407540388da80c8e930b64729ddcb5ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.twig", "delete.twig", 1);
        $this->blocks = array(
            'javaScript' => array($this, 'block_javaScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["title"] = "Todolist - Delete";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_javaScript($context, array $blocks = array())
    {
        // line 6
        echo "        <script src=\"js/delete.js\"></script>
    ";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
        <div class=\"box\" id=\"boxCompleteTodo\">

            <h2>Complete todo</h2>

            <div class=\"boxInner\">
                <p>Are you sure you want to complete the todo <strong>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute(($context["todo"] ?? null), "what", array()), "html", null, true);
        echo "</strong>?</p>
                <form action=\"";
        // line 17
        echo twig_escape_filter($this->env, ($context["deleteUrl"] ?? null), "html", null, true);
        echo "\" method=\"post\">
                    <fieldset class=\"columns\">
                        <label class=\"column column-12 cancel alignLeft\"><a href=\"index.php\" title=\"Cancel and go back\">Cancel and go back</a></label>
                        <label for=\"btnSubmit\" class=\"column column-12 alignRight\"><input type=\"submit\" id=\"btnSubmit\" name=\"btnSubmit\" value=\"Complete\" /></label>
                        <input type=\"hidden\" name=\"id\" value=\" ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["todo"] ?? null), "id", array()), "html", null, true);
        echo "\" />
                        <input type=\"hidden\" name=\"moduleAction\" id=\"moduleAction\" value=\"delete\" />
                    </fieldset>
                </form>
            </div>

        </div>

        ";
        // line 29
        $this->loadTemplate("partials/formerrors.twig", "delete.twig", 29)->display($context);
        // line 30
        echo "    ";
    }

    public function getTemplateName()
    {
        return "delete.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 30,  73 => 29,  62 => 21,  55 => 17,  51 => 16,  43 => 10,  40 => 9,  35 => 6,  32 => 5,  28 => 1,  26 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "delete.twig", "D:\\wamp64\\www\\PHP\\2018-19\\labo5_LorenzoLongo\\labo5\\labo4_LorenzoLongo\\labo4\\basisbestanden\\www_\\templates\\delete.twig");
    }
}

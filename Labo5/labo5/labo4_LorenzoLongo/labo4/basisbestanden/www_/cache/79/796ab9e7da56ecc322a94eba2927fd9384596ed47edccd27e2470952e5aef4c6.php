<?php

/* edit.twig */
class __TwigTemplate_0226cd0df9f078c538ac1ae8971767a36b9b7a476e5c836e67f0bd354cdddc4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.twig", "edit.twig", 1);
        $this->blocks = array(
            'javaScript' => array($this, 'block_javaScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["title"] = "Todolist - Edit";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_javaScript($context, array $blocks = array())
    {
        // line 6
        echo "\t\t<script src=\"js/edit.js\"></script>
\t";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
\t\t\t<div class=\"box\" id=\"boxAddTodo\">

\t\t\t\t<h2>Edit existing todo</h2>

\t\t\t\t<div class=\"boxInner\">
\t\t\t\t\t<form action=\"";
        // line 16
        echo twig_escape_filter($this->env, ($context["editUrl"] ?? null), "html", null, true);
        echo "\" method=\"post\">
\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t<dl class=\"clearfix columns\">
\t\t\t\t\t\t\t\t<dd class=\"column column-46\"><input type=\"text\" name=\"what\" id=\"what\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, ($context["what"] ?? null), "html", null, true);
        echo "\" /></dd>
\t\t\t\t\t\t\t\t<dd class=\"column column-16\" id=\"col-priority\">
\t\t\t\t\t\t\t\t\t<select name=\"priority\" id=\"priority\">
\t\t\t\t\t\t\t\t\t\t";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["priorities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["prior"]) {
            // line 23
            echo "\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $context["prior"], "html", null, true);
            echo "\" ";
            if ((($context["priority"] ?? null) == $context["prior"])) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $context["prior"], "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prior'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t\t<dd class=\"column column-16\" id=\"col-submit\">
\t\t\t\t\t\t\t\t\t<label for=\"btnSubmit\"><input type=\"submit\" id=\"btnSubmit\" name=\"btnSubmit\" value=\"Edit\" /></label>
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute(($context["todo"] ?? null), "id", array()), "html", null, true);
        echo "\" />
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"moduleAction\" id=\"moduleAction\" value=\"edit\" />
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t</fieldset>
\t\t\t\t\t</form>
\t\t\t\t\t<p class=\"cancel\">or <a href=\"index.php\" title=\"Cancel and go back\">Cancel and go back</a></p>
\t\t\t\t</div>

\t\t\t</div>

\t";
        // line 40
        $this->loadTemplate("/partials/formerrors.twig", "edit.twig", 40)->display($context);
        // line 41
        echo "
\t";
    }

    public function getTemplateName()
    {
        return "edit.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 41,  102 => 40,  88 => 29,  82 => 25,  67 => 23,  63 => 22,  57 => 19,  51 => 16,  43 => 10,  40 => 9,  35 => 6,  32 => 5,  28 => 1,  26 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "edit.twig", "D:\\wamp64\\www\\PHP\\2018-19\\labo5_LorenzoLongo\\labo5\\labo4_LorenzoLongo\\labo4\\basisbestanden\\www_\\templates\\edit.twig");
    }
}

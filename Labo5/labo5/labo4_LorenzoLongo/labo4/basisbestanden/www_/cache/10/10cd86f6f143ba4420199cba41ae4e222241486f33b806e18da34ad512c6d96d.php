<?php

/* browse.twig */
class __TwigTemplate_bd4464680bdc068d715ca52f422c2fab044802cfda4cdfb2b29ccfee21650afc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout.twig", "browse.twig", 1);
        $this->blocks = array(
            'javaScript' => array($this, 'block_javaScript'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        $context["title"] = "Todolist";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_javaScript($context, array $blocks = array())
    {
        // line 6
        echo "\t    <script src=\"js/browse.js\"></script>
    ";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "\t\t\t<div class=\"box\" id=\"boxAddTodo\">

\t\t\t\t<h2>Add new todo</h2>

\t\t\t\t<div class=\"boxInner\">
\t\t\t\t\t<form action=\"";
        // line 15
        echo twig_escape_filter($this->env, ($context["browseUrl"] ?? null), "html", null, true);
        echo "\" method=\"post\">
\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t<dl class=\"clearfix columns\">
\t\t\t\t\t\t\t\t<dd class=\"column column-46\"><input type=\"text\" name=\"what\" id=\"what\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, ($context["what"] ?? null), "html", null, true);
        echo "\" /></dd>
\t\t\t\t\t\t\t\t<dd class=\"column column-16\" id=\"col-priority\">
\t\t\t\t\t\t\t\t\t<select name=\"priority\" id=\"priority\">
                                        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["priorities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["prior"]) {
            // line 22
            echo "\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $context["prior"], "html", null, true);
            echo "\" ";
            if ((($context["priority"] ?? null) == $context["prior"])) {
                echo " selected=\"selected\" ";
            }
            echo ">";
            echo twig_escape_filter($this->env, $context["prior"], "html", null, true);
            echo "</option>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prior'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t\t<dd class=\"column column-16\" id=\"col-submit\">
\t\t\t\t\t\t\t\t\t<label for=\"btnSubmit\"><input type=\"submit\" id=\"btnSubmit\" name=\"btnSubmit\" value=\"Add\" /></label>
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"moduleAction\" id=\"moduleAction\" value=\"add\" />
\t\t\t\t\t\t\t\t</dd>
\t\t\t\t\t\t\t</dl>
\t\t\t\t\t\t</fieldset>
\t\t\t\t\t</form>
\t\t\t\t</div>

\t\t\t</div>

    \t";
        // line 37
        $this->loadTemplate("/partials/formerrors.twig", "browse.twig", 37)->display($context);
        // line 38
        echo "
\t\t\t<div class=\"box\" id=\"boxYourTodos\">

\t\t\t\t<h2>Your todos</h2>

\t\t\t\t<div class=\"boxInner\">

                    ";
        // line 45
        if ((twig_length_filter($this->env, ($context["todos"] ?? null)) > 0)) {
            // line 46
            echo "\t\t\t\t\t<ul>
                        ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["todos"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["todo"]) {
                // line 48
                echo "\t\t\t\t\t\t<li id=\"item-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "id", array()), "html", null, true);
                echo "\" class=\"item ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "priority", array()), "html", null, true);
                echo " clearfix\"><a href=\"delete.php?id";
                echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "id", array()), "html", null, true);
                echo "\" class=\"delete\" title=\"Delete/Complete this item\">delete/complete</a><a href=\"edit.php?id=";
                echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "id", array()), "html", null, true);
                echo "\" class=\"edit\" title=\"Edit this item\">edit</a><span>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["todo"], "what", array()), "html", null, true);
                echo "</span></li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['todo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "\t\t\t\t\t</ul>
                    ";
        } else {
            // line 52
            echo "\t\t\t\t\t<p>No todos found!</p>
                    ";
        }
        // line 54
        echo "
\t\t\t\t</div>
\t\t\t</div>

\t";
    }

    public function getTemplateName()
    {
        return "browse.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 54,  137 => 52,  133 => 50,  116 => 48,  112 => 47,  109 => 46,  107 => 45,  98 => 38,  96 => 37,  81 => 24,  66 => 22,  62 => 21,  56 => 18,  50 => 15,  43 => 10,  40 => 9,  35 => 6,  32 => 5,  28 => 1,  26 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "browse.twig", "D:\\wamp64\\www\\PHP\\2018-19\\labo5_LorenzoLongo\\labo5\\labo4_LorenzoLongo\\labo4\\basisbestanden\\www_\\templates\\browse.twig");
    }
}

<?php

	/**
	 * Includes
	 * ----------------------------------------------------------------
	 */


		// config & functions
		require_once 'includes/config.php';
		require_once 'includes/functions.php';

        require_once __DIR__ . '/includes/Twig/Autoloader.php';
        Twig_Autoloader::register();

        $loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
        $twig = new Twig_Environment($loader, array('cache' => __DIR__ . '/cache', 'auto_reload' => DEBUG ));



/**
	 * Database Connection
	 * ----------------------------------------------------------------
	 */

		// @TODO
        $db = getDbConnection();


	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */


		$priorities = array('low','normal','high'); // The possible priorities of a todo
		$formErrors = array(); // The encountered form errors

		$what = isset($_POST['what']) ? $_POST['what'] : ''; // The todo that was sent in via the form
		$priority = isset($_POST['priority']) ? $_POST['priority'] : 'low'; // The priority that was sent in via the form
        $moduleAction = isset($_POST['moduleAction']) ? $_POST['moduleAction'] : '';


	/**
	 * Handle action 'add' (user pressed add button)
	 * ----------------------------------------------------------------
	 */

		if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'add')) {
		    $allOk = true;
		    $insertOK = false;
			// check parameters

				// @TODO (if an error was encountered, add it to the $formErrors array)
                if(trim($what) === '') {
                    $allOk = false;
                    $formErrors[] = 'Please insert a name for your todo!';
                }

                if(!in_array($priority ,$priorities)) {
                    $allOk = false;
                    $formErrors[] = 'Invalid priority chosen!';
                }
			// if no errors: insert values into database

				// @TODO
                if($allOk === true) {
                    try {
                        $insert = $db->prepare('INSERT INTO todolist (what, priority, added_on) VALUES (?, ?, ?)');
                        $insert->execute(array($what, $priority, (new DateTime())->format('Y-m-d H:i:s')));
                        $insertOK = true;
                    } catch (Exception $e) {
                        showDbError('failedInsert', $e->getMessage());
                    }
                }

			// if query succeeded: redirect to this very same page
                if($insertOK === true) {
                    header('location: browse.php');
                }


		}


	/**
	 * No action to handle: show our page itself
	 * ----------------------------------------------------------------
	 */

		// Fetch needed data from DB

			// @TODO get all todo items from databases
            $stmt = $db->prepare('SELECT * FROM todolist ORDER BY priority, what DESC');
            $stmt->execute();
            $collectionToDo = $stmt->fetchAll(PDO::FETCH_ASSOC);



            $tpl = $twig->loadTemplate('browse.twig');
            echo $tpl->render(array(
                    'browseUrl' => $_SERVER['PHP_SELF'],
                    'what' => $what,
                    'priorities' => $priorities,
                    'priority' => $priority,
                    'formErrors' => $formErrors,
                    'todos' => $collectionToDo
            ));
?>
<?php

    // @TODO
    require_once 'includes/config.php';
    require_once 'includes/functions.php';

    require_once __DIR__ . '/includes/Twig/Autoloader.php';
    Twig_Autoloader::register();

    $loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
    $twig = new Twig_Environment($loader, array('cache' => __DIR__ . '/cache', 'auto_reload' => DEBUG ));


    $db = getDbConnection();

    $formErrors = array();
    $id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

    if (isset($_POST['moduleAction']) && ($_POST['moduleAction'] == 'delete')) {

        $id = isset($_POST['id']) ? (int) $_POST['id'] : 0;

        $stmt = $db->prepare('SELECT COUNT(*) FROM todolist WHERE id = ?');
        $stmt->execute(array($id));
        $numItems = $stmt->fetchColumn();
        if ($numItems != 1) {
            header('location: browse.php');
            exit();
        }

        if (sizeof($formErrors) == 0) {

            $stmt = $db->prepare('DELETE FROM todolist WHERE id = ?');
            $stmt->execute(array($id));

            if ($stmt->rowCount() != 0) {
                header('location: browse.php');
                exit();
            }
            else {
                $formErrors[] = 'Error while deleting the item. Please retry.';
            }
        }
    }

    $stmt = $db->prepare('SELECT * FROM todolist WHERE id = ?');
    $stmt->execute(array($id));
    if ($stmt->rowCount() != 1) {
        header('location: browse.php');
        exit();
    }

    $item = $stmt->fetch(PDO::FETCH_ASSOC);

    $template = $twig->loadTemplate('delete.twig');
    echo $template->render(array(
                'todo' => $item,
                'deleteUrl' => $_SERVER['PHP_SELF'] . '?id=' . $id,
                'formErrors' => $formErrors
    ));
?>
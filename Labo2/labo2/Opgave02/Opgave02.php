<?php
/**
 * Lab02 - Exercise 02
 * User: lorenzo.longo
 * Date: 19/11/2018
 * Time: 15:53
 */

$number1 = (int)isset($_GET['nrOne']) ? $_GET['nrOne'] : rand(1, 100);
$number2 = (int)isset($_GET['nrTwo']) ? $_GET['nrTwo'] : rand(1, 100);
$result = 0;

$msgFirstNr = '*';
$msgSecondNr = '*';

$moduleAction = isset($_GET['moduleAction']) ? $_GET['moduleAction'] : '';

if($moduleAction == 'processName') {
    $allOK = true;

    if (!filter_var($number1, FILTER_VALIDATE_INT)) {
        $msgFirstNr = 'Please enter a number';
        $allOK = false;
    }

    if (!filter_var($number2, FILTER_VALIDATE_INT)) {
        $msgSecondNr = 'Please enter a number';
        $allOK = false;
    }


    if ($allOK === true) {
        $result = $number2 + $number1;
    }
}

?><!DOCTYPE html>
<html>
<head>
    <title>Opgave02</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
    <fieldset>

        <h2>Testform #1</h2>

        <dl class="clearfix">
            <dt><label for="name">Number 1</label></dt>
            <dd class="text">
                <input type="text" id="nrOne" name="nrOne" value="<?php echo htmlentities($number1); ?>" class="input-text" />
                <span class="message error"><?php echo $msgFirstNr; ?></span>
            </dd>
            <dt><label for="name">Number 2</label></dt>
            <dd class="text">
                <input type="text" id="nrTwo" name="nrTwo" value="<?php echo htmlentities($number2); ?>" class="input-text" />
                <span class="message error"><?php echo $msgSecondNr; ?></span>
            </dd>
            <dt><label for="name">Result</label></dt>
            <dd class="text">
                <input type="text" id="result" name="result" value="<?php echo htmlentities($result); ?>" class="input-text" disabled/>
            </dd>

            <dt class="full clearfix" id="lastrow">
                <input type="hidden" name="moduleAction" value="processName" />
                <input type="submit" id="btnSubmit" name="btnSubmit" value="Send" />
            </dt>
        </dl>

    </fieldset>
</form>
</body>
</html>


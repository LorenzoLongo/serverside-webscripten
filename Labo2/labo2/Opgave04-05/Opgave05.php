<?php
/**
 * Lab02, Exercise 05
 * User: Lorenzo Longo
 * Date: 23/11/2018
 * Time: 15:35
 */
$queries = array();

parse_str($_SERVER['QUERY_STRING'], $queries);

?><!DOCTYPE html>
<html>
<head>
    <title>Opgave04</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>
    <p>Thank you, <?php  echo $queries['name']; ?> ! We've send a confirmation mail to <?php echo $queries['mail']; ?> !</p>
</body>
</html>
<?php
/**
 * Lab02, Exercise 04
 * User: lorenzo.longo
 * Date: 19/11/2018
 * Time: 17:46
 */

// Show all errors (for educational purposes)
ini_set('error_reporting', E_ALL);

// Get all variables
$name = isset($_POST['name']) ? (string) $_POST['name'] : '';
$mail = isset($_POST['mail']) ? (string) $_POST['mail'] : '';
$company = isset($_POST['company']) ? (string) $_POST['company'] : '';
$country = isset($_POST['country']) ? (int) $_POST['country'] : 0;
$preference = isset($_POST['preference']) ? (string) $_POST['preference'] : '';
$restrictions = isset($_POST['restrictions']) ? (string) $_POST['restrictions'] : '';

$moduleAction = isset($_POST['moduleAction']) ? $_POST['moduleAction'] : '';


$err_name = $err_mail = $err_company = $err_country = $err_preference = $err_restrictions = '';

if($moduleAction == 'processName') {
    $allOk = true;

    if(trim($name) === '') {
        $allOk = false;
        $err_name = 'Please insert a name';
    }

    if(trim($mail) === '' && !filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        $allOk = false;
        $err_mail = 'Please insert a valid mail address';
    }

    if(trim($company) === '') {
        $allOk = false;
        $err_company = 'Please fill in a company name';
    }

    if($country === 0) {
        $allOk = false;
        $err_country = 'Please select a country';
    }

    if(trim($preference) === '') {
        $allOk = false;
        $err_preference = 'Please select your preferable conference';
    }

    if(trim($restrictions) === '') {
        $allOk = false;
        $err_restrictions = 'Please fill in all food restrictions';
    }

    if($allOk === true) {
        header('Location:Opgave05.php?name=' .urlencode($name) . '&mail=' . urlencode($mail));
        exit(0);
    }
}

?><!DOCTYPE html>
<html>
<head>
    <title>Opgave04</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

    <fieldset>

        <h2>Opgave04</h2>

        <dl class="clearfix">
            <p>Schrijf je in voor onze conferentie:</p>
            <dt><label for="name">Name</label></dt>
            <dd class="text">
                <input type="text" id="name" name="name" value="<?php echo htmlentities($name); ?>" class="input-text" />
                <p><span class="message error"><?php echo $err_name; ?></span></p>
            </dd>

            <dt><label for="email">Mail</label></dt>
            <dd class="text">
                <input type="email" id="mail" name="mail" value="<?php echo htmlentities($mail); ?>" class="input-text" />
                <p><span class="message error"><?php echo $err_mail; ?></span></p>
            </dd>

            <dt><label for="company">Company</label></dt>
            <dd class="text">
                <input type="text" id="company" name="company" value="<?php echo htmlentities($company); ?>" class="input-text" />
                <p><span class="message error"><?php echo $err_company; ?></span></p>
            </dd>

            <dt><label for="country">Country</label></dt>
            <dd>
                <select name="country" id="country">
                    <option value="0"<?php if (htmlentities($country) == 0) { echo ' selected="selected"'; } ?>>Please select...</option>
                    <option value="1"<?php if (htmlentities($country) == 1) { echo ' selected="selected"'; } ?>>België</option>
                    <option value="2"<?php if (htmlentities($country) == 2) { echo ' selected="selected"'; } ?>>Nederland</option>
                    <option value="3"<?php if (htmlentities($country) == 3) { echo ' selected="selected"'; } ?>>Frankrijk</option>
                    <option value="4"<?php if (htmlentities($country) == 4) { echo ' selected="selected"'; } ?>>Duitsland</option>
                    <option value="5"<?php if (htmlentities($country) == 5) { echo ' selected="selected"'; } ?>>Italië</option>
                    <option value="6"<?php if (htmlentities($country) == 6) { echo ' selected="selected"'; } ?>>Spanje</option>
                </select>
                <p><span class="message error"><?php echo $err_country; ?></span></p>
            </dd>

            <dt><label>Preference</label></dt>
            <dd>
                <label for="server"><input type="radio" class="option" name="preference" id="server" value="server-side"<?php if (htmlentities($preference) == 'server-side') { echo ' checked="checked"'; } ?> />server-side</label>
                <label for="client"><input type="radio" class="option" name="preference" id="client" value="client-side"<?php if (htmlentities($preference) == 'client-side') { echo ' checked="checked"'; } ?> />client-side</label>
                <label for="fullstack"><input type="radio" class="option" name="preference" id="fullstack" value="fullstack"<?php if (htmlentities($preference) == 'fullstack') { echo ' checked="checked"'; } ?> />fullstack</label>
                <p><span class="message error"><?php echo $err_preference; ?></span></p>
            </dd>

            <dt><label for="restrictions">Food Restrictions</label></dt>
            <dd class="text">
                <textarea name="restrictions" id="restrictions" rows="5" cols="40"><?php echo htmlentities($restrictions); ?></textarea>
                <p><span class="message error"><?php echo $err_restrictions; ?></span></p>
            </dd>

            <dt class="full clearfix" id="lastrow">
                <input type="hidden" name="moduleAction" value="processName" />
                <input type="submit" id="btnSubmit" name="btnSubmit" value="Send" />
            </dt>

        </dl>

    </fieldset>

</form>
</body>
</html>
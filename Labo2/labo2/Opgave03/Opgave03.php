<?php
/**
 * Lab02, Exercise 03
 * User: lorenzo.longo
 * Date: 19/11/2018
 * Time: 16:18
 */

$prices = array(
    '64GB' => 19,
    '128GB' => 33,
    '256GB' => 62
);

$stickSize = isset($_POST['sticks']) ? $_POST['sticks'] : '';
$moduleAction = isset($_POST['moduleAction']) ? $_POST['moduleAction'] : '';

$msgPrice = '';

if($moduleAction == 'processName') {
    $allOk = true;

    if(array_key_exists($stickSize, $prices)) {
        $msgPrice = 'De prijs is ' . $prices[$stickSize] . ' euro';
    } else {
        $allOk = false;
        $msgPrice = 'Select a USB size!';
    }
}

?><!DOCTYPE html>
<html>
<head>
    <title>Opgave03</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <h2>Opgave03</h2>

        <dl class="clearfix">
            <fieldset>
                <dt><label>USB Sticks</label></dt>
                <dd>
                    <?php
                        foreach($prices as $type => $price) {
                            echo '<label><input type="radio" class="option" name="sticks" id="sticks" value="'. $type .'" ' .($type == $type) . '/>' . $type . '</label><br />';
                        }
                    ?>
                </dd>

                <dt class="full clearfix" id="lastrow">
                    <input type="hidden" name="moduleAction" value="processName" />
                    <input type="submit" id="btnSubmit" name="btnSubmit" value="Send" />
                </dt>
            </fieldset>
        </dl>
    </form>
    <p><span class="message error"><?php echo $msgPrice; ?></span></p>
</body>
</html>

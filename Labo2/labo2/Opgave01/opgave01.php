<?php
/**
 * Lab02, Exercise 01
 * User: lorenzo.longo
 * Date: 19/11/2018
 * Time: 15:31
 */
error_reporting(E_ALL);
ini_set('display_errors','on');

$name = isset($_GET['name']) ? $_GET['name'] : '';

$moduleAction = isset($_GET['moduleAction']) ? $_GET['moduleAction'] : '';

$msgName = '*';

if($moduleAction == 'processName') {
    $allOK = true;

    if (trim($name) === '') {
        $msgName = 'Please enter a name';
        $allOK = false;
    }

    if ($allOK === true) {
        header('Location: opgave01.php?name=' .urlencode($name));
        exit();
    }
}

?><!DOCTYPE html>
<html>
<head>
    <title>Opgave01</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
        <fieldset>

            <h2>Testform #1</h2>

            <dl class="clearfix">
                <dt><label for="name">Name</label></dt>
                <dd class="text">
                    <input type="text" id="name" name="name" value="<?php echo htmlentities($name); ?>" class="input-text" />
                    <span class="message error"><?php echo $msgName; ?></span>
                </dd>

                <dt class="full clearfix" id="lastrow">
                    <input type="hidden" name="moduleAction" value="processName" />
                    <input type="submit" id="btnSubmit" name="btnSubmit" value="Send" />
                </dt>
            </dl>

        </fieldset>
    </form>
</body>
</html>
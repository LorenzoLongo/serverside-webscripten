<?php
/**
 * Lab03, Exercise 04
 * User: lorenzo.longo
 * Date: 26/11/2018
 * Time: 16:15
 */

    $caption = isset($_POST['caption']) ? $_POST['caption'] : '';

    $msgCaption = '*';

    $basePath = __DIR__ . DIRECTORY_SEPARATOR . 'images';
    $baseUrl = 'images';

    $files = new DirectoryIterator($basePath);
    $x = 0;

    foreach($files as $file) {
        $x += 1;
    }
    $x = $x -3;


        if(trim($caption) === '') {
            $msgCaption = 'Please insert a caption';
        } else {
            file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'images\captions.txt', PHP_EOL . $caption , FILE_APPEND);
        }

        if (isset($_FILES['avatar']) && ($_FILES['avatar']['error'] === UPLOAD_ERR_OK)) {
            if (in_array((new SplFileInfo($_FILES['avatar']['name']))->getExtension(), array('jpeg', 'jpg'))) {

                $moved = @move_uploaded_file($_FILES['avatar']['tmp_name'], $baseUrl . DIRECTORY_SEPARATOR . ++$x . '.jpg');
                if ($moved) {
                    header('Location:opgave_2.php');
                } else {
                    echo('<p>Error while saving file in the uploads folder</p>');
                }
            } else {
                echo('<p>Invalid extension. Only .jpg allowed</p>');
            }
        }

?><!DOCTYPE html>
<html>
<head>
    <title>Lab03 - Opgave04</title>
    <meta charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="css/styles.css" />
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data" >
        <fieldset>
            <dl>
                <dt><label for="caption">Caption</label></dt>
                <dd>
                    <input type="text" id="caption" name="caption" value="<?php echo htmlentities($caption); ?>" class="input-text" />
                    <span class="message error"><?php echo $msgCaption; ?></span>
                </dd>

                <dd>
                    <input type="file" name="avatar" id="avatar" /><input type="submit" />
                </dd>
            </dl>
        </fieldset>
    </form>
</body>
</html>





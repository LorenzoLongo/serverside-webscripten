<?php
        $currentPath = isset($_GET['path'])  ? $_GET['path'] : '';
        $basePath = __DIR__ . DIRECTORY_SEPARATOR . $currentPath;
        $directories = [];
        $files = [];

        $startDirectory = new DirectoryIterator($basePath);

        foreach($startDirectory as $memberOfDirectory) {
            if($memberOfDirectory->isDir() && !$memberOfDirectory->isDot()) {
                $directories[] = array(
                    'folder' => 'isFolder',
                    'name' => (string)$memberOfDirectory,
                    'url' => $currentPath . '/' . $memberOfDirectory
                );
            } else if ($memberOfDirectory->isFile()) {
                $files[] = array(
                    'file' => 'isFile',
                    'name' => (string)$memberOfDirectory,
                    'extension' => $memberOfDirectory->getExtension(),
                    'fileSize' => convert_filesize($memberOfDirectory->getSize()),
                    'url' => $currentPath . '/' . $memberOfDirectory
                );
            }
        }

        function convert_filesize($bytes, $decimals = 2){
            $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
            $factor = floor((strlen($bytes) - 1) / 3);
            return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
        }

?><!DOCTYPE html>
<html>
<head>
	<title>Lab03 - Exercise 05</title>
	<meta charset="utf-8" />
	<style>
		ul {
			margin: 0;
			padding: 0;
		}
		li {
			list-style: none;
			display: block;
			height: 24px;
			line-height: 24px;
			font-family: monospace;
            margin-bottom: 0.2rem;
		}

		li:nth-child(2n) {
			background: rgba(0,0,0,0.05);
		}

		li:hover {
			background: #c2e1ff;
		}

		li img {
			margin-right: 4px;
			position: relative;
			top: 4px;
		}

        li.isFolder {
            background-image: url(/icons/folder.gif);
            background-repeat: no-repeat;
        }

        li.avi {
            background-image: url(icons/avi.gif);
            background-repeat: no-repeat;
        }

        li.bmp {
            background-image: url(icons/bmp.gif);
            background-repeat: no-repeat;
        }

        li.isFile {
            background-image: url(icons/default.gif);
            background-repeat: no-repeat;
        }

        li.dll {
            background-image: url(icons/dll.gif);
            background-repeat: no-repeat;
        }

        li.doc {
            background-image: url(icons/doc.gif);
            background-repeat: no-repeat;
        }

        li.gif {
            background-image: url(icons/gif.gif);
            background-repeat: no-repeat;
        }

        li.html {
            background-image: url(icons/html.gif);
            background-repeat: no-repeat;
        }

        li.jpg {
            background-image: url(icons/jpg.gif);
            background-repeat: no-repeat;
        }

        li.js {
            background-image: url(icons/js.gif);
            background-repeat: no-repeat;
        }

        li.mp3 {
            background-image: url(icons/mp3.gif);
            background-repeat: no-repeat;
        }

        li.pdf {
            background-image: url(icons/pdf.gif);
            background-repeat: no-repeat;
        }

        li.php {
            background-image: url(icons/php.gif);
            background-repeat: no-repeat;
        }

        li.png {
            background-image: url(icons/png.gif);
            background-repeat: no-repeat;
        }

        li.ppt {
            background-image: url(icons/ppt.gif);
            background-repeat: no-repeat;
        }

        li.swf {
            background-image: url(icons/swf.gif);
            background-repeat: no-repeat;
        }

        li.txt {
            background-image: url(icons/txt.gif);
            background-repeat: no-repeat;
        }

        li.up {
            background-image: url(icons/up.gif);
            background-repeat: no-repeat;
        }

        li.vsd {
            background-image: url(icons/vsd.gif);
            background-repeat: no-repeat;
        }

        li.xls {
            background-image: url(icons/xls.gif);
            background-repeat: no-repeat;
        }

        li.xml {
            background-image: url(icons/xml.gif);
            background-repeat: no-repeat;
        }

        li.zip {
            background-image: url(icons/zip.gif);
            background-repeat: no-repeat;
        }

        a {
            margin-left: 2rem;
            margin-right: 1rem;
        }

	</style>
</head>
<body>

	<h1>Browsing <?php echo $basePath ?></h1>

	<ul>
        <?php
            foreach($directories as $dir) {
                echo '<li class="' . $dir['folder'] . '"><a href="' . $_SERVER['PHP_SELF'] . '?path=' . urlencode($dir['url']) . '">' . $dir['name'] .'</a></li>';
            }

            foreach($files as $file) {
                echo '<li class="' . $file['file'] . ' ' . $file['extension'] . '"><a href="' . $file['url'] . '">' . $file['name'] .'</a>' .  '    ' . $file['fileSize'] . '</li>' . PHP_EOL;
            }
        ?>
	</ul>

</body>
</html>
<?php
/**
 * Lab01, Exercise 05
 * User: Lorenzo Longo
 * Date: 16/11/2018
 * Time: 19:21
 */

// CHECK PARAMETERS
if ($argc < 2) {
    echo 'insuffient parameters!';
    exit();
}

// VARIABLES
$sentence = $argv[1];
$words = [];
$frequentions = [];


// REMOVE ALL PUNCTUATIONS AND SET ALL LETTERS TO LOWERCASE
$sentence = preg_replace('/[^a-z0-9]+/i', ' ', $sentence);
$sentence = strtolower($sentence);

// TRIM SENTENCE INTO WORDS (ARRAY)
$words = explode(' ', $sentence);


// COUNT ARRAY VALUES THAT ARE DUPLICATES AND SORT
$frequentions = array_count_values($words);
arsort($frequentions);


// PRINT RESULTING ARRAY
var_dump($frequentions);

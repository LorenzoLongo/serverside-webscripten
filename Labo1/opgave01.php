<?php
/**
 * Lab01, Exercise 01
 * User: lorenzo.longo
 * Date: 12/11/2018
 * Time: 15:34
 */

$firstName = 'Lorenzo';
$lastName = 'Longo';

echo 'Hello World!' . PHP_EOL;
echo 'It\'s raining outside' . PHP_EOL;
echo 'The value of $firstName is ' . $firstName . '. The value of $lastName is ' . $lastName . PHP_EOL;
<?php
/**
 * Lab01, Exercise 03
 * User: lorenzo.longo
 * Date: 12/11/2018
 * Time: 16:41
 */

// CHECK PARAMETERS
if ($argc < 2) {
    echo 'insuffient parameters!';
    exit();
}

// SET TIMEZONE
date_default_timezone_set('Europe/Brussels');


// CONVERT THE INPUT TO A DATE
$date = $argv[1];
$date = strtotime($date);

// PRINT OUT DATE
if ($date !== false) {
    echo $date . PHP_EOL;
    echo date('l' ,$date) . PHP_EOL;
    echo date('dmY' , $date) . PHP_EOL;
    echo date('g:i A', $date) . PHP_EOL;
    echo date('j F, Y', $date) . PHP_EOL;
}




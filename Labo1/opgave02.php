<?php
/**
 * Lab01, Exercise 02
 * User: lorenzo.longo
 * Date: 12/11/2018
 * Time: 15:46
 */

// ALPHABET ARRAY

$alphabet = array();

for ($x = 97; $x < 123 ;$x++) {
    $alphabet[] = chr($x);
}

var_dump($alphabet);


// SHOW ARRAY ALPHABET WITH FOREACH

foreach($alphabet as $key => $value) {
    echo $key + 1 . $value . PHP_EOL;
}


// IMPLODE ALPHABET ARRAY

echo implode(',', $alphabet) . PHP_EOL;


// DELETE FIRST VALUE OF ALPHABET ARRAY + CHECK LENGTH

echo sizeof($alphabet) . PHP_EOL;

array_shift($alphabet);

echo sizeof($alphabet) . PHP_EOL;


// DEFINE CITIES ARRAY

$cities = [ 9000 => 'GENT', 1000 => 'BRUSSEL', 2000 => 'ANTWERPEN', 8500 => 'KORTRIJK', 3000 => 'LEUVEN', 3500 => 'HASSELT' ];


// PUT KEY IN ZIP ARRAY AND PRINT TOTAL SUM OF ZIPS

$zips = array_keys($cities);
var_dump($zips);


// SUM OF ZIP CODES

echo array_sum($zips) . PHP_EOL;


// $CITIES ARRAY ALPHABETICAL SORT

asort($cities);
var_dump($cities);


// $CITIES ARRAY KEYS SORT

ksort($cities);
var_dump($cities);

// TERNARY OPERATOR

for($i = 0; $i < 10000; $i += 1000) {
    $existingZip = array_key_exists($i, $cities) ? true : false;
    if($existingZip === true) {
        echo $cities[$i] . PHP_EOL;
    }
}
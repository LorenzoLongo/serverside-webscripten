<?php

    // @TODO: Insert showDbError Here
    function showDbError($type, $msg) {
        if(DEBUG === 'true') {
             switch($type) {
                 case 'connect':
                     echo 'error connecting to DB!';
                     break;
                 case 'failedInsert':
                     echo 'Failed to execute the insert query';
                     break;
                 default:
                     echo 'no errors!';
                     break;
             }
        } else {
            file_put_contents(__DIR__ . '/ error_log_mysql',PHP_EOL . (new DateTime())->format('Y-m-d H:i:s') . ' : ' . $msg, FILE_APPEND);
            header('location: error.php?type=db&detail=' . $type);
        }
        exit();

    }

    function getDbConnection() {
        try {
            $db = new PDO('mysql:host=' . DB_HOST .';dbname=' . DB_NAME .';charset=utf8mb4', DB_USER, DB_PASS);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $db;
        } catch (PDOException $e) {
            showDbError('connect', $e->getMessage());
        }
    }
// EOF